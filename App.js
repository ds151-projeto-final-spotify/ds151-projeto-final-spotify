// In App.js in a new project

import * as React from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import HomeScreen from './src/screens/HomeScreen';
import { navigationRef } from './src/navigation/RootNavigation';
import LoginScreen from './src/screens/LoginScreen'
import DrawerTab from './src/screens/DrawerTab'
import { SafeAreaView, SafeAreaProvider } from 'react-native-safe-area-context';
import {AuthProvider} from './src/context/AuthContext'

const Stack = createStackNavigator();


function App() {
    
    
    return(
    <SafeAreaProvider>    
    <AuthProvider>        
    <NavigationContainer ref={navigationRef} >
    
        <Stack.Navigator>
            <Stack.Screen  name="Login" options={{headerStyle : styles.Header }} component={LoginScreen} />     
            <Stack.Screen  name="Drawer" component={DrawerTab} 
            options={{headerShown:false}}
            />
        </Stack.Navigator>
        
    </NavigationContainer>
    </AuthProvider> 
    </SafeAreaProvider>
    );
}

export default App;

const styles = StyleSheet.create({
    Header: {
      backgroundColor: '#1DB954', 
    }
  });

