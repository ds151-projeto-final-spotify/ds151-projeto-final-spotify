import  React, {useContext, useEffect, useState} from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { AuthContext } from '../context/AuthContext';
import Spotify from '../api/Spotify';
import { SafeAreaView } from 'react-native-safe-area-context';


function HomeScreen() {
   const { authState, dataUser } = useContext(AuthContext);
   const [user,setUser] = useState({});
   useEffect(()=>{
    async function getUser(){
      try{
        const response = await Spotify.get('me',{
          params : {
            
          }
        });
        dataUser(response.data)
        setUser(response.data)
      }catch(exception){
    
        console.log(exception);
    
      } 
    }
    getUser();
  
  },[]);
  
  if(user.images !== undefined){
    return (
      <SafeAreaView  style={styles.Screen} > 
      <Image style={styles.Image} source={{
        uri: `${user.images[0].url }`
        }}/>
      <Text style={styles.Label}>Bem vindo {user.display_name }</Text>
   </SafeAreaView>
    );
      }else{
        return(
          <SafeAreaView  style={styles.Screen} > 
            <Text style={styles.Label}>Carregando...</Text>
         </SafeAreaView>
        )
      }
  }

export default HomeScreen;

const styles = StyleSheet.create({
  Image: {
      borderRadius: 100,
      marginLeft: 30,
      marginRight: 30,
      backgroundColor: '#1db954',
      height: 200,
      width: 200,
      alignItems: 'center',
      justifyContent: 'center',
      maxWidth: '80%',
  },
  Label: {
      color: '#1DB954',
      margin: 'auto',
      fontWeight: 'bold',
      fontSize: 20,
  },
  Screen: {
    backgroundColor: '#262626',
    flex: 1, 
    alignItems: 'center'
  }
});