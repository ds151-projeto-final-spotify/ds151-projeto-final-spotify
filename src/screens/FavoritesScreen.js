import  React, {useContext, useEffect, useState} from 'react';
import { View, Text, Image, FlatList, StyleSheet } from 'react-native';
import { AuthContext } from '../context/AuthContext';
import Spotify from '../api/Spotify';
import { SafeAreaView } from 'react-native-safe-area-context';

function FavoritesScreen(){
    const[list,setList] = useState({})

    useEffect(()=>{
        async function getListFavorites(){
          try{
            const response = await Spotify.get('me/tracks?market=BR',{
              params : {
                
              }
            });
        //  console.log(response.data.items[0])
            setList(response.data)
                        
          }catch(exception){
        
            console.log(exception);
        
          } 
        }
        getListFavorites();
      
      },[]);
     
      if(list.items !== undefined){  
    return(
        <SafeAreaView style={styles.Screen}>
            <FlatList
            data={list.items}
            keyExtractor={item => item.track.id}
            renderItem={({item}) => {
             
                return(
                <View style={styles.Item}>
                    <Image style={styles.Image} source={{
                        uri: `${item.track.album.images[1].url }`
                    }}/>
                    <Text style={styles.Label}>{item.track.name}</Text>
                   
                </View>
              )
            }}
            />     
        </SafeAreaView>
    )
        }else{
            return(
                <SafeAreaView style={styles.Screen}>
                    <Text style={styles.Label}>Carregando...</Text>
                </SafeAreaView>    
            )
        } 
}

export default FavoritesScreen;

const styles = StyleSheet.create({
  Image: {
      borderRadius: 30,
      marginLeft: 10,
      marginRight: 20,
      marginBottom: 10,
      marginTop:10,
      height: 100,
      width: 100,
      alignItems: 'center',
      justifyContent: 'center',
      maxWidth: '80%',
  },
  Label: {
      color: '#00FF7F',
      margin: 'auto',
      fontSize: 20,
      marginTop: 35,
    
  },
  Item:{
     flex : 1,
    flexDirection: 'row',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  Screen: {
    backgroundColor: '#262626',
    flex: 1, 
    alignItems: 'center'
  }
});