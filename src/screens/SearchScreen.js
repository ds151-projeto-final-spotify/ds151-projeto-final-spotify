import  React, {useContext, useEffect, useState} from 'react';
import { View, Text, Image, FlatList, StyleSheet } from 'react-native';
import { AuthContext } from '../context/AuthContext';
import Spotify from '../api/Spotify';
import { SafeAreaView } from 'react-native-safe-area-context';
import SearchBar from '../components/SearchBar';

function SearchScreen(){
    const { authState } = useContext(AuthContext);
    const [text,setText] = useState('')
    const [results,setResults] = useState({});
    

    async function search(query){
        try{
         const response = await Spotify.get(`search?type=track&q="${query}"&market=BR&include_external=audio`,{
           params : {
             
           }
         });
         setResults(response.data.tracks.items);
        //  console.log(response.data.tracks.items)
       }catch(exception){
     
         console.log(exception);
     
       }  
    }
    return(
        <SafeAreaView style={styles.Screen}>
            <SearchBar
            value={text}
            onTextChange={(t)=>setText(t)}
            onTextSubmit={(t)=> search(t)}
            placeholder="Busque por uma musica"
            />
        
       <FlatList
            data={results}
            keyExtractor={item => item.id.toString()}
            renderItem={({item}) => {
             
                return(
                <View style={styles.Item}>
                    <Image style={styles.Image} source={{
                        uri: `${item.album.images[1].url }`
                    }}/>
                    <Text style={styles.Label}>{item.name}</Text>
                </View>
              )
            }}
            />
        </SafeAreaView>
    )
}

export default SearchScreen;

const styles = StyleSheet.create({
  Image: {
      borderRadius: 30,
      marginLeft: 10,
      marginRight: 20,
      marginBottom: 10,
      marginTop:10,
      // borderWidth: 2,
      // backgroundColor: '#1db954',
      height: 100,
      width: 100,
      alignItems: 'center',
      justifyContent: 'center',
      maxWidth: '80%',
  },
  Label: {
      color: '#00FF7F',
      margin: 'auto',
      fontSize: 20,
      marginTop: 35,
    
  },
  Item:{
    flexDirection: 'row',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  Screen: {
    backgroundColor: '#262626',
    flex: 1, 
    alignItems: 'center'
  }
});