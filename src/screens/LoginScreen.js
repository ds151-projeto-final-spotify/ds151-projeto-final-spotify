import React, {useContext,useState, useEffect} from 'react';
import * as WebBrowser from 'expo-web-browser';
import { makeRedirectUri, useAuthRequest } from 'expo-auth-session';
import { Text, Button, StyleSheet, TouchableOpacity } from 'react-native';
import base64 from 'react-native-base64'
import axios from 'axios';
import { AuthContext } from '../context/AuthContext';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Entypo } from '@expo/vector-icons'; 


const qs = require('qs');
const discovery = {
  authorizationEndpoint: 'https://accounts.spotify.com/authorize',
  tokenEndpoint: 'https://accounts.spotify.com/api/token',
};

export default function LoginScreen() {
  const { signIn, tryLocalSiginIn} = useContext(AuthContext);

  const [request, response, promptAsync] = useAuthRequest(
    {
      clientId: 'c3016ac620fb4a57b5d5ecd064c05c81',
      scopes: ['user-read-email', 'playlist-modify-private', 'playlist-modify-public', 'user-follow-modify', 'user-read-recently-played', 'user-library-read', 'user-top-read'],
      usePKCE: false,
      redirectUri: makeRedirectUri({}),
    },
    discovery
  );

  useEffect(() => {
    if (response?.type === 'success') {
      const { code } = response.params;
     
      spotifyGetKey(code)
      }
  }, [response]);

  useEffect(()=>{
    tryLocalSiginIn();
   },[]);

  async function spotifyGetKey(code) {
    const client = base64.encode('c3016ac620fb4a57b5d5ecd064c05c81:f5ff24811f3d4f538c89063971d93aec')
    let data = ''
        data = qs.stringify({
            'grant_type': 'authorization_code',
            'redirect_uri': makeRedirectUri({}),
            'code': code
         })
     
    await axios.post('https://accounts.spotify.com/api/token',
        data = data,
        {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Basic ${client}`
            }
        })
        .then(result => {

                signIn(result.data.access_token)
        })
        .catch((error) => console.error("access_token ", error))
};
  return (
    <SafeAreaView  style={styles.Screen} >
     <TouchableOpacity
        disabled={!request}
        style={styles.Button}
        onPress={() => {
          promptAsync();
          }}
      >
        <Text style={styles.Label}>Login com Spotify</Text>
        <Entypo name="spotify" size={20} color="black" />
      </TouchableOpacity>
   </SafeAreaView> 
  );
}


const styles = StyleSheet.create({
  Label:{
    margin: 'auto',
    fontWeight: 'bold',
    marginRight: 10,
  },
  Button: {
      height: 50,
      borderRadius: 50,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#1DB954',
      margin: 'auto',
      fontWeight: 'bold',
      fontSize: 20,
      flexDirection: 'row',

  },
  Screen: {
    justifyContent: 'flex-start',
    backgroundColor: '#262626',
    flex: 1, 
  }
});