import React, { useContext } from 'react';
import {StyleSheet} from 'react-native';
import {
    createDrawerNavigator,
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
  } from '@react-navigation/drawer';
import  HomeScreen  from './HomeScreen';
import SearchScreen from './SearchScreen';
import FavoritesScreen from './FavoritesScreen';
import { AuthContext } from '../context/AuthContext';


const Drawer = createDrawerNavigator();

function CustomDrawerContent(props) {
    const { signOut} = useContext(AuthContext);
  
    return (
        <DrawerContentScrollView {...props}>
          <DrawerItemList {...props} />
          <DrawerItem label="LogOut" onPress={() => signOut()}
            labelStyle = {{
              color: 'red'
            }}
          
          />
        </DrawerContentScrollView>
      );
    }
  

function DrawerTab() {
    return (
        
        <Drawer.Navigator   screenOptions={{drawerStyle: styles.Drawer, 
                            drawerActiveBackgroundColor : '#696969',
                            drawerInactiveTintColor: '#1DB954',
                            drawerActiveTintColor: '#1DB954',
                          }} initialRouteName = "Home"
        drawerContent={(props) => <CustomDrawerContent {...props} />}
        >
          <Drawer.Screen options={{headerStyle : styles.Header }} name="Home" component={HomeScreen} />
          <Drawer.Screen options={{headerStyle : styles.Header }} name="Search" component={SearchScreen}/>
          <Drawer.Screen options={{headerStyle : styles.Header }} name="Favorites" component={FavoritesScreen}/>
        </Drawer.Navigator>
            
    );
  }

  export default DrawerTab;

  const styles = StyleSheet.create({
    Header: {
      backgroundColor: '#1DB954', 
    },
    Drawer : {
      backgroundColor: '#262626',
      width: 240,
    }
  });