import * as React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native' 
import { Feather } from '@expo/vector-icons'; 



export default function SearchBar({onTextChange, onTextSubmit, value, placeholder}){
    return(
        <View style={styles.view}>
            <Feather name="search" size={20} color="black" />
            <TextInput 
            autoCapitalize="none"
            autoCorrect={false}
            placeholder={placeholder}
            style={styles.input}
            value = {value}
            onChangeText = {newText => onTextChange(newText) } 
            onEndEditing ={() => onTextSubmit(value)}
             />
             
        </View>
    );


}

const styles = StyleSheet.create({
    view: {
      height: 40,
      margin: 12,
      borderWidth: 3,
      padding: 5,
      flexDirection: 'row',
      backgroundColor: 'lightgray', 
    },
    input: {
      marginHorizontal: 5,
      flex: 1
    },

  });