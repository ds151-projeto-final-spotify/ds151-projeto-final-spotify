import React, {createContext, useReducer} from 'react';
import axios from 'axios';
import * as RootNavigation from '../navigation/RootNavigation'
import AsyncStorage from '@react-native-async-storage/async-storage'


const AuthContext = createContext();

function authReducer(state, action){
    switch(action.type){
        case "signIn":
            console.log("Login");
            return({
                ...state,
                signedIn: true,
                access_token: action.payload,
            });
        case "error":
            return({
                ...state,
                error: action.payload
            });
        case "dataUser":
            return({
                ...state,
                dataUser: action.payload
            })        
        case "signOut":
            console.log("Logout");
            return({
                ...state,
                signedIn: false,
                access_token: null,
                dataUser: null,
            });        
        default:
            return({...state});
    }
}

const AuthProvider = ({children}) => {

    const [authState, dispatch] = useReducer(authReducer, {
        signedIn: false,
        access_token: null,
        error: '',
        dataUser: null,
    })
    
    const signOut = async () => {
        try{
        await AsyncStorage.removeItem('access_token')
        await AsyncStorage.removeItem('dataUser')
        }catch(error){
             dispatch({type: 'error', payload: 'Erro no logout, '+error})
        }
        dispatch({type: 'signOut'});
        RootNavigation.navigate("Login");
    };

    const tryLocalSiginIn = async () => {
        const access_token = await AsyncStorage.getItem('access_token');
        await AsyncStorage.removeItem('dataUser')
        if(access_token){
        dispatch({type: 'signIn', payload: access_token})
        RootNavigation.navigate('Drawer');
        }else{
            dispatch({type: 'signOut'});
            RootNavigation.navigate('Login');
        }
    }
    const dataUser = async (dataUser) => {
        try {
            const qs = require('qs');
            // console.log(qs.parse(qs.stringify(dataUser)))            
            await AsyncStorage.setItem("datauser", qs.stringify(dataUser))
            dispatch({type: 'dataUser', payload: dataUser})
       } catch (error) {
           dispatch({type: 'error', payload: 'Erro nos dados do usuario '+error})    
       }
    }
    const signIn = async (access_token) => {
        try {

             await AsyncStorage.setItem('access_token', access_token)
             dispatch({type: 'signIn', payload: access_token})
             RootNavigation.navigate("Drawer");
        } catch (error) {
            dispatch({type: 'error', payload: 'Erro na autenticação'+error})    
        }
    };

    return(

        <AuthContext.Provider value={{
            authState,
            signIn,
            tryLocalSiginIn,
            signOut,
            dataUser,
        }}>
            {children}
        </AuthContext.Provider>
    );
    };

export {AuthContext, AuthProvider};
